import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template:`<h2>{{date}}</h2>
  <h2>{{date | date:'short'}}</h2>
  <h2>{{date | date:'shortDate'}}</h2>
  
  `
  ,
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

public name = "Powerfit";
public message = "Exercise builds";
public date = new Date();


  constructor() { }

  ngOnInit(): void {
  }

}
